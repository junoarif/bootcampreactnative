import React from 'react'
import { StyleSheet, 
        Platform, 
        TouchableOpacity, 
        Button, 
        ScrollView, 
        Image, 
        Text, 
        TextInput,
        KeyboardAvoidingView,
        Linking,
        View } from 'react-native'

const LoginScreen = () => {
    return (
        <KeyboardAvoidingView
        behavior = {Platform.OS =="ios" ? "padding" : "height"}
        style={styles.container}
        >
            <ScrollView>
                <View style={styles.containerView}>
                    
                    <Image source={require('./assets/logo.png')} style={styles.logo} />
                    
                    <Text style={styles.logintext}>Login</Text>

                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Username/Email</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}/>
                    </View>
                    <View style={styles.kotakBtn}>
                        <View style={styles.autotext}>
                            <TouchableOpacity>
                                <text  onPress={() => Linking.openURL('http://google.com')}>Forget Password</text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.kotaklogin}>
                            <TouchableOpacity style={styles.btreg}>
                                <Text style={styles.textbt}>Sign up</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btlogin}>
                                <Text style={styles.textbt}>Sign in</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: "#ff7f00"
    },
    logo:{
        height: 168,
        width: 168,
        top: 30,
        alignSelf: 'center'

    },
    logintext:{
        fontSize: 36,
        marginTop: 63,
        textAlign: 'center',
        color: '#FFFFFF',
        marginVertical:20
    },
    containerView:{
        alignSelf: 'center'

    },
    forminput:{
        marginHorizontal: 30,
        marginVertical: 5,
        width: 295,
    },
    formtext:{
        color: '#FFFFFF',
        padding: 5
    },
    autotext:{
        //fontSize: 20,
        color: '#5F3606',
        padding: 10,
        textAlign: 'right'
    },
    input:{
        height: 40,
        borderColor: "#003366",
        backgroundColor: "#E29B5A",
        padding: 10,
        borderRadius: 10,
        color : "#FFFFFF"
    },
    kotakBtn:{
        alignSelf: 'center',
        flexDirection: 'raw',
        justifyContent: 'space-around'
    },
    kotaklogin:{
        alignContent: 'center',
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10,
        width: 295,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    btlogin:{
        alignItems: 'center',
        backgroundColor: "#4798E3",
        padding: 10,
        borderRadius: 10,
        width: 140
    },
    btreg:{
        alignItems: 'center',
        //backgroundColor: "#003366",
        borderWidth: 1,
        borderColor: "#FFFFFF",
        padding: 10,
        borderRadius: 10,
        width: 140
    },
    textbt:{
        color: "#FFFFFF",
        fontSize: 20
    }
    
})
