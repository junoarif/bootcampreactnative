import React from 'react'
import { StyleSheet,
    Platform,
    TouchableOpacity,
    Button,
    ScrollView,
    Image,
    Text,
    TextInput,
    KeyboardAvoidingView,
    Linking,
    View} from 'react-native'

    import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const AboutScreen = () => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.kotakHeader}>
                    <Text style={styles.judul}> About Me</Text>
                </View>

                <View style={styles.kotakAbout}>
                    <View style={styles.aboutLogo}>
                        <FontAwesome5 
                            name="user-circle"
                            size={100} solid
                            color="#EFEFEF"
                            style={styles.icon}
                        />
                    </View>

                    <View style={styles.aboutName}>
                        <Text style={styles.nama}>Arif Herjuno Anwar Fadholi</Text>
                        <Text style={styles.kerjaan}>Tukang Ketik</Text>
                    </View>
                </View>
                
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Portofolio</Text>
                    <View style={styles.kotakDalem}>
                        <View>
                            <FontAwesome5 name="gitlab" size={40} color="#4798E3" style={styles.icon}/>
                            <Text style={styles.textDalam}>@junoarif</Text>
                        </View>

                        <View>
                            <FontAwesome5 name="github" size={40} style={styles.icon}/>
                            <Text style={styles.textDalam}>@junoarif</Text>
                        </View>
                    </View>

                </View>

                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Contact Me</Text>

                    <View style={styles.kotakdalamver}>

                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <FontAwesome5 name="facebook" size={40} style={styles.icon}/>
                            </View>
                            <View style={styles.textName}> 
                                <Text style={styles.textDalam}>@junoarif</Text>
                            </View>
                        </View>

                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <FontAwesome5 name="instagram" size={40} style={styles.icon} />
                            </View>
                            <View style={styles.textName}>
                                <Text style={styles.textDalam}>@junoarif</Text>
                            </View>
                        </View>

                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <FontAwesome5 name="twitter" size={40} style={styles.icon}/>
                            </View>
                            <View style={styles.textName}>
                                <Text style={styles.textDalam}>@junoarif</Text>
                            </View>
                        </View>
                    </View>

                </View>
            </View>
        </ScrollView>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container:{
        flex: 1
        //backgroundColor: '#ff7f00'
    },
    kotakHeader:{
        backgroundColor: "#ff7f00"
    },
    kotakAbout:{
        flexDirection: "row",
        justifyContent: 'space-around'
    },
    aboutLogo:{
        margin: 10
    },
    aboutName:{
        margin: 10,
        justifyContent: 'center'
    },
    kotak:{
        borderColor: "#ff7f00",
        borderRadius: 10,
        //borderBottomColor: "#000000",
        padding: 5,
        //backgroundColor: "#ff7f00",
        marginBottom: 9

    },
    kotakdalamverhub:{
        margin: 5,
        marginTop: 10,
        flexDirection: "row",
        justifyContent: 'center'

    },
    kotakdalamver:{
        borderTopWidth: 2,
        borderTopColor: "#ff7f00",
        flexDirection: "column",
        justifyContent: 'space-around'

    },
    kotakDalem:{
        borderTopWidth: 2,
        borderTopColor: "#ff7f00",
        flexDirection: "row",
        justifyContent: 'space-around'
    },
    nama:{
        fontSize: 24,
        fontWeight: 'bold',
        color: "#5F3606",
        textAlign: 'center'
    },
    kerjaan:{
        fontSize: 16,
        fontWeight: 'bold',
        color: "#3EC6FF",
        textAlign: "center",
        marginBottom: 7
    },
    judul:{
        fontSize: 36,
        fontWeight: "bold",
        color: "#5F3606",
        textAlign: 'center'
    },
    juduldalam:{
        fontSize: 16,
        color: "#ff7f00"
    },
    icon:{
        textAlign: 'center'
    },
    textDalam:{
        fontSize: 16,
        fontWeight: 'light',
        color: "#B22222",
        textAlign: "center"
    },
    textName:{
        justifyContent: 'center',
        marginLeft: 10,
        color: "#FFFFFF"
    }

})
