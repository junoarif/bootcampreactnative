import React from 'react'
import { 
    StyleSheet, 
    Text, 
    Platform,
    TouchableOpacity,
    Button,
    ScrollView,
    Image,
    TextInput,
    KeyboardAvoidingView,
    Linking,
    View } from 'react-native'

const RegisterScreen = () => {
    return (
        <KeyboardAvoidingView
        behavior = {Platform.OS =="ios" ? "padding" : "height"}
        style={styles.container}
        >
            <ScrollView>
                <View style={styles.containerView}>
                    {/*<Image source={require('./assets/logo.png')} style={styles.logo} /> */}
                    <Text style={styles.judul}>Registration</Text>

                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Username</Text>
                        <TextInput style={styles.input} />
                    </View>

                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Email</Text>
                        <TextInput style={styles.input} />
                    </View>

                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>No.HP</Text>
                        <TextInput style={styles.input} />
                    </View>

                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}/>
                    </View>

                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Re-type Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}/>
                    </View>

                    <View style={styles.kotakBtn}>
                        <TouchableOpacity style={styles.btnLogin}>
                            <Text style={styles.textbt}>Sign in</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnSubmit}>
                            <Text style={styles.textbt}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: "#ff7f00"
    },
    containerView:{
        alignSelf: 'center'

    },
    judul:{
        fontSize: 36,
        marginTop: 63,
        textAlign: 'center',
        color: '#FFFFFF',
        marginVertical:20
    },
    forminput:{
        marginHorizontal: 30,
        marginVertical: 5,
        width: 295,
    },
    input:{
        height: 40,
        borderColor: "#003366",
        backgroundColor: "#E29B5A",
        padding: 10,
        borderRadius: 10,
        color : "#FFFFFF"
    },
    formtext:{
        color: '#FFFFFF',
        padding: 5
    },
    kotakBtn:{
        aligContent: 'center',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        marginBottom: 10,
        width: 295
    },
    btnSubmit:{
        alignItems: 'center',
        backgroundColor: "#4798E3",
        padding: 10,
        borderRadius: 10,
        width: 140
    },
    btnLogin:{
        alignItems: 'center',
        //backgroundColor: "#003366",
        borderWidth: 1,
        borderColor: "#FFFFFF",
        padding: 10,
        borderRadius: 10,
        width: 140
    },
    textbt:{
        color: "#FFFFFF",
        fontSize: 20
    }
    
})

