// Soal No.2 (Promise Baca Buku)
console.log('\n == Soal No.2 (Promise Baca Buku) == \n')

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]


async function actReadBookWithPromise() {
    let waktu = 10000
    for (let i = 0; i < books.length; i++){
        waktu = await readBooksPromise(waktu, books[i])
            .then(function (sisaWaktu) {
                return sisaWaktu;
            })
            .catch( function (sisaWaktu) {
                return sisaWaktu;
            })
    }
    console.log('selesai')
}

actReadBookWithPromise()