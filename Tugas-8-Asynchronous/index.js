// Soal No.1 (Callback Baca Buku)
console.log('\n == Soal No.1 (Callback Baca Buku) == \n')

var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

let waktu = 10000
let i = 0

function actReadBook() {
    readBooks(waktu, books[i], function(sisaWaktu) {
        waktu = sisaWaktu
        i++
        if (i < books.length) {
            actReadBook()
        }
    })
}

actReadBook()

readBooks(10000, books[0], function(sisaWaktu) {
    
})
