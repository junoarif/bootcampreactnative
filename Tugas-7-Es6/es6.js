// 1. Mengubah fungsi menjadi fungsi arrow
console.log('\n == 1. Mengubah fungsi menjadi fungsi arrow == \n')

const golden = goldenFunction = () => {
    console.log("this is golden!!")
}

golden()

// 2. Sederhanakan menjadi Object literal di ES6
console.log('\n == 2. Sederhanakan menjadi Object literal di ES6 == \n')

const newFunction = function literal(firstName, lastName){
    return {firstName, lastName, 
        fullName: function(){
            console.log(firstName + " " + lastName)
            return
        } 
    }
}
   
//Driver Code 
newFunction("William", "Imoh").fullName() 


// 3. Destructuring
console.log('\n == 3. Destructuring == \n')

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)


// 4. Array Spreading
console.log('\n == 4. Array Spreading == \n')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)
const combined = [...west, ...east]

//Driver Code
console.log(combined)

// 5. Template Literals
console.log('\n == 5. Template Literals == \n')

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet,  
    consectetur adipiscing elit, ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`
 
// Driver Code
console.log(before) 