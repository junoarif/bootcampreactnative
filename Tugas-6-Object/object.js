// Soal No.1 (Array to Object)
console.log('\n == Soal No.1 (Array to Object) == \n')

function arrayToObject(arr){
    var now = new Date()
    var thisYear = now.getFullYear() // (tahun sekarang)
    var obj = {}
    if(arr == null) {
        console.log('Tidak ada data yang di prosess')
    } else if(arr == []){
        console.log('')
    } else {
        for(i = 0; i < arr.length; i++){
            var str = ''
            var tmpAge =''
            // menggabungkan index 0 dan 1 
            str += arr[i][0] + ' ' + arr[i][1]
            // cek apakah tahun sekarang lebih kecil dari tahun lahir
            if(thisYear < arr[i][3]) {
                tmpAge = 'Invalid birth year'
            } else {
                tmpAngka = thisYear - arr[i][3]
            }
            // asign ke object
            obj[str] = {firstName:arr[i][0],
                        lastName:arr[i][1],
                        gender:arr[i][2],
                        age:tmpAge
            }
        }
    }

    console.log(obj);
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
// Error case 
arrayToObject([]) // ""



// Soal No.2 (Shopping Time)
console.log('\n == Soal No.2 (Shopping Time) == \n')

function shoppingTime(memberId, money) {
    // item sale
    var sale    = [
        { category: 'Sepatu', brand: 'Stacattu', prize: 1500000}, 
        { category: 'Baju', brand: 'Zoro', prize: 500000},
        { category: 'Baju', brand: 'H&N', prize: 250000},
        { category: 'Sweeter', brand: 'Uniklooh', prize: 175000},
        { category: 'Casing Handphone', prize: 50000}
    ]
    // shorting item sale by prize descending (dari yang termahal)
    var saleSort = sale.sort(function (a, b) {
        return b.prize - a.prize;
    });
    // variable untuk menamopung shoppingcart
    var shoppingCart = {}

    // handling parameter
    if(memberId == null || memberId == '') {
        shoppingCart = 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }else if (money < 50000 || money == null) {
        shoppingCart = 'Mohon maaf, uang tidak cukup'
    }else {
        // core program
        // isi object
        shoppingCart.memberId = memberId
        shoppingCart.money = money
        // isi shopping cart dengan pembelian dari harga ter mahal
        tempMoney = money
        for(i = 0; i < saleSort.length; i ++){
            while(tempMoney < 50000){
                if(money < i.prize) {
                    tempMoney -= 0;
                }else{
                    tempMoney -= i.prize;
                    shoppingCart.listPurchased = i;
                }
            }
        }
        shoppingCart.changeMoney = tempMoney;
    }

    return shoppingCart
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// Soal No.3 (Naik Angkot)
console.log('\n == Soal No.3 (Naik Angkot) == \n')

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //var tempBayar = 0;
    var report = []
    for(i = 0; i < arrPenumpang.length; i++){
        // deklarasi object penumpang
        var penumpang = {}
        penumpang.penumpang = arrPenumpang[i][0]
        penumpang.naikDari = arrPenumpang[i][1]
        penumpang.tujuan = arrPenumpang[i][2]
        penumpang.bayar = 0
        // iterasi bayar berdasarkan naikDari sampai dengan tujuan 
        for(a = rute.indexOf(arrPenumpang[i][1]); a < rute.indexOf(arrPenumpang[i][2]); a++){
            penumpang.bayar += 2000;
        }
        // push object penumpang ke array report
        report.push(penumpang)
    }

    return report;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]