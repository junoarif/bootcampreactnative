// if-else
console.log('\n == If-else ==')
var name = "juno"
var peran = "Guard"

if ( name == "" && peran == "") {
    console.log('Nama harus diisi!')
} else if (name != "" && peran != "") {
    console.log('Selamat datang di dunia Werewolf, ' + name)
    if (peran == "Penyihir") {
        console.log('Halo ' + peran + ' ' + name + ', kamu dapat melihat siapa yang menjadi werewolf!')
    } else if (peran == "Guard") {
        console.log('Halo ' + peran + ' ' + name + ', kamu akan membantu melindungi temanmu dari serangan werewolf!')
    } else if (peran == "Werewolf") {
        console.log('Halo ' + peran + ' ' + name + ', Kamu akan memakan mangsa setiap malam!')
    } else {
        console.log('Halo ' + peran + ' ' + name + ', Maaf, Peran yang kamu pilih tidak terdaftar!')
    }
} else if (name != "" && peran == "") {
    console.log('Halo ' + name + ', Pilih peranmu untuk memulai game!')
} else {
    console.log("Error!")
}

// Switch
console.log('\n == Switch ==')

var hari = 21; 
var bulan = 1; 
var tahun = 1945;

switch(bulan) {
    case 1: { console.log(hari + ' Januari ' + tahun); break; }
    case 2: { console.log(hari + ' Februari ' + tahun); break; }
    case 3: { console.log(hari + ' Maret ' + tahun); break; }
    case 4: { console.log(hari + ' April ' + tahun); break; }
    case 5: { console.log(hari + ' Mei ' + tahun); break; }
    case 6: { console.log(hari + ' Juni ' + tahun); break; }
    case 7: { console.log(hari + ' Juli ' + tahun); break; }
    case 1: { console.log(hari + ' Agustus ' + tahun); break; }
    case 1: { console.log(hari + ' September ' + tahun); break; }
    case 1: { console.log(hari + ' Oktober ' + tahun); break; }
    case 1: { console.log(hari + ' November ' + tahun); break; }
    case 1: { console.log(hari + ' Desember ' + tahun); break; }
    default: { console.log('Tidak terjadi apa-apa'); }
}