// Soal No.1 (Range)
console.log('\n == Soal No.1 (Range) == \n')

function range(startNum, finishNum){
    var temp=[];
    if (startNum > finishNum){
        for(i=startNum;i>=finishNum; i--){
            temp.push(i);
        }
    }else if (startNum < finishNum){
        for(i=startNum;i<=finishNum; i++){
            temp.push(i);
        }
    }else if (startNum ==  null || finishNum == null){
        temp.push(-1)
    }
    return temp
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal No.2 (Range With Step)
console.log('\n == Soal No.2 (Range with step) == \n')

function rangeWithStep(startNum, finishNum, step){
    var temp=[];
    if (startNum ==  null || finishNum == null || step == null){
        temp.push(-1)
    }else {
        if (startNum > finishNum){
            for(i=startNum;i>=finishNum; i-=step){
                temp.push(i);
            }
        }else if (startNum < finishNum){
            for(i=startNum;i<=finishNum; i+=step){
                temp.push(i);
            }
        }
    }
    return temp
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal No.3 (Sum of Range)
console.log('\n == Soal No.3 (Sum of Range) == \n')

function sum(startNum, finishNum, step){
    var temp=[];
    var total=0;
    if (startNum ==  null && finishNum == null){
        temp.push(0)
    }else if (startNum ==  null || finishNum == null) {
        temp.push(1)
    }else if (step == null){
        step = 1;
        if (startNum > finishNum){
            for(i=startNum;i>=finishNum; i-=step){
                temp.push(i);
            }
        }else if (startNum < finishNum){
            for(i=startNum;i<=finishNum; i+=step){
                temp.push(i);
            }
        }
    }else if (step != null) {
        if (startNum > finishNum){
            for(i=startNum;i>=finishNum; i-=step){
                temp.push(i);
            }
        }else if (startNum < finishNum){
            for(i=startNum;i<=finishNum; i+=step){
                temp.push(i);
            }
        }
    }

    for (i = 0; i < temp.length; i++){
        total += temp[i];
    }

    return total;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No.4 (Array Multidimensi)
console.log('\n == Soal No.4 (Array Multidimensi) == \n')

function dataHandling(arg) {
    var parse='';
    if (arg == null) {
        parse = 'tidak ada data yang di proses';
    } else {
        for (i = 0; i < arg.length; i++){
            parse += 'Nomor ID : ' + arg[i][0] + '\n';
            parse += 'Nama Lengkap : ' + arg[i][1] + '\n';
            parse += 'TTL : ' + arg[i][2] + ' ' + arg[i][3] + '\n';
            parse += 'Hobi : ' + arg[i][4] + '\n';
            parse += '\n';
        }
    }

    //return parse;
    console.log(parse);
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

//console.log(dataHandling(input));
dataHandling(input);




// Soal No.5 (Balik Kata)
console.log('\n == Soal No.5 (Balik Kata) == \n')

function balikKata(param){
    var temp = '';
    var temp_array = [];

    if (param == null) {
        temp = 'Tidak ada data yang di proses';
    }else {
        //parsing string to array
        for(i = 0; i <= param.length; i++){
            temp_array.push(param.charAt(i));
        }
        // balik urutan karakter kemudian kembalikan ke string didalam var temp
        for(a = (temp_array.length -1); a >= 0; a--){
            temp += temp_array[a];
        }
    }
    return temp;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No.6 (Metode Array)
console.log('\n == Soal No.6 (Metode Array) == \n')


function dataHandling2(arr){
    var temp = '';
    var temp_array = [];
    if(arr == null){
        temp = "Tidak ada data yang di proses"
    }else {
        temp_array = arr.splice(0, 6, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
        temp += temp_array +'\n';

        var date = temp_array[3];
        var date_array = date.split('/');
        switch(date_array[1]) {
            case '01': { temp += 'Januari \n'; break; }
            case '02': { temp += 'Februari \n'; break; }
            case '03': { temp += 'Maret \n'; break; }
            case '04': { temp += 'April \n'; break; }
            case '05': { temp += 'Mei \n'; break; }
            case '06': { temp += 'Juni \n'; break; }
            case '07': { temp += 'Juli \n'; break; }
            case '08': { temp += 'Agustus \n'; break; }
            case '09': { temp += 'September \n'; break; }
            case '10': { temp += 'Oktober \n'; break; }
            case '11': { temp += 'November \n'; break; }
            case '12': { temp += 'Desember \n'; break; }
            default: { console.log('Tidak ada bulan');}
        }

        temp += date_array.sort(function (value1, value2) { return value2 - value1 } ) + '\n';

        temp += date_array.join('-') + '\n';

        var temp_str = temp_array.slice(1,2)
        var str = String(temp_str)
        temp += str.slice(0,15)

    }

    console.log(temp)
}

var input = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]

dataHandling2(input);