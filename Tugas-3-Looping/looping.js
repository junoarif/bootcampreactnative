// No. 1 
console.log('== No.1 Looping While == \n')

var row = 0;

console.log('LOOPING PERTAMA')
while ( row < 20 ){
    row += 2;
    console.log(row + ' - I love coding')
}
console.log('LOOPING KEDUA')
console.log(row + ' - I love coding')
while ( row > 2 ){
    row -= 2;
    console.log(row + ' - I love coding')
}


// No. 2
console.log('\n == No.2 Looping For == \n')

for (var angka = 1; angka <= 20; angka ++){
    if (angka %2 != 0){
        if (angka %3 == 0){
            console.log(angka + ' - I Love Coding')
        }else {
            console.log(angka + ' - Santai')
        }
    }else if (angka %2 == 0){
        console.log(angka + ' - Berkualitas')
    }
}

// No. 3
console.log('\n == No. 3 Membuat Persegi Panjang  == \n')

var a = '';
for (var b = 1; b <= 4; b ++){
    for (var c = 1; c <= 8; c++){
        a += '#';
    } 
    a += '\n';
}
console.log(a);

// No. 4
console.log('\n == No. 4 Membuat Tangga  == \n')

var s = '';
for (var i= 1; i <= 7; i ++){
    for (var k = 1; k <= i; k++){
        s += '#';
    } 
    for (var l = 1; l <= i-1; l++){
        s += '#';
    }
    s += '\n';
}
console.log(s);

// No. 5
console.log('\n == No. 5 Membuat Papan Catur  == \n')

var ct = '';
for (var x = 1; x <= 8; x ++){
    if (x %2 != 0) {
        for (var y = 1; y <= 8; y++){
            if (y %2 ==0){
                ct += '#';
            }else{
                ct += ' ';
            }   
        }  
    }else if (x %2 == 0){
        for (var y = 1; y <= 8; y++){
            if (y %2 ==0){
                ct += ' ';
            }else{
                ct += '#';
            }   
        } 
    }
    ct += '\n';
}
console.log(ct);
